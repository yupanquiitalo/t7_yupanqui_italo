﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01_yupanqui_italo
{
    class Juguete
    {
        public string name;
        public float price;
        public int age;
        public bool storage;

        public Juguete(string name, float price, int age, bool storage) 
        {
            this.name = name;
            this.price = price;
            this.age = age;
            this.storage = storage;
        }

        public virtual void status()
        {
            Console.WriteLine(name + " cuesta " + price + "$ es para niños de " + age + " años y " + storage + " tiene almacenamiento.");
        }
    }
}
