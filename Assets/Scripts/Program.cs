﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Con la parte de Dornitorio hacer un programa que le permita al usuario escoger un articulo de una lista y agregarlo a su carrito de compras. Al final mostrar los productos seleccionados por el usuario.

namespace P01_yupanqui_italo
{
    class Item
    {
        public string name;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Descanso O1 = new Lampara ("Lámpara", 20, 12, false);
            Descanso O2 = new Almohada  ("Almohada", 10, 5, false);
            Descanso O3 = new Cuna  ("Cuna", 50, 2, false);
            Almacenaje O4 = new MesaDeNoche ("Mesa de Noche", 20, 12, true);
            Almacenaje O5 = new Ropero ("Ropero", 100, 12, true);

            Console.WriteLine("Productos de Dormitorio de Juguete: (0 para finalizar la compra)");

            Item[] PR = new Item[5];
            PR[0] = new Item
            {
                name = "Lámpara"
            };
            PR[1] = new Item
            {
                name = "Almohada"
            };
            PR[2] = new Item
            {
                name = "Cuna"
            };
            PR[3] = new Item
            {
                name = "Mesa de Noche"
            };
            PR[4] = new Item 
            {
                name = "Ropero"
            };

            int flag;
            flag = -1;
            List<Item> lista = new List<Item>();
            while (flag != 0)
            {
                for (int x = 0; x < PR.Length; x++)
                {
                    Console.WriteLine("Introduce " + (x + 1) + " para añadir " + PR[x].name + " al carrito.");

                }
                flag = Convert.ToInt32(Console.ReadLine());
                if (flag > 0 && flag <= PR.Length)
                {
                    lista.Add(PR[flag - 1]);
                }
            }
            for (int x = 0; x < lista.Count; x++)
            {
                Console.WriteLine(lista[x].name + " se ha añadido al carrito.");
            }
        }
    }
}
