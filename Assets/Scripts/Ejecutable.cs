using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using P01_yupanqui_italo;

public class Ejecutable : MonoBehaviour
{
    private string y;
    private string z;
    class Item
    {

        public string name;
    }

    void Start()
    {

        Descanso O1 = new Lampara("L�mpara", 20, 12, false);
        Descanso O2 = new Almohada("Almohada", 10, 5, false);
        Descanso O3 = new Cuna("Cuna", 50, 2, false);
        Almacenaje O4 = new MesaDeNoche("Mesa de Noche", 20, 12, true);
        Almacenaje O5 = new Ropero("Ropero", 100, 12, true);

        Call();

        Debug.Log("Productos de Dormitorio de Juguete: (0 para finalizar la compra)");
        y = "5";
        switch (y)
        {
            case "1":
                O1.status();
                End();
                break;
            case "2":
                O2.status();
                End();
                break;
            case "3":
                O3.status();
                End();
                break;
            case "4":
                O4.status();
                Call();
                End();
                break;
        }
    }

    void Call()
    {

        Debug.Log("Productos:");
        Item[] PR = new Item[5];
            PR[0] = new Item
            {
                name = "L�mpara"
            };
            PR[1] = new Item
            {
                name = "Almohada"
            };
            PR[2] = new Item
            {
                name = "Cuna"
            };
            PR[3] = new Item
            {
                name = "Mesa de Noche"
            };
            PR[4] = new Item 
            {
                name = "Ropero"
            };

        int flag;
        flag = -1;
        List<Item> lista = new List<Item>();
        while (flag != 0)
        {
            for (int x = 0; x < PR.Length; x++)
            {

                Debug.Log("Introduce " + (x + 1) + " para a�adir " + PR[x].name + " al carrito.");
            }
            flag = Convert.ToInt32(Console.ReadLine());
            if (flag > 0 && flag <= PR.Length)
            {
                lista.Add(PR[flag - 1]);
            }
        }

        for (int x = 0; x < lista.Count; x++)
        {
            Debug.Log(lista[x].name + " se ha a�adido al carrito.");
        }
    }


    void End()
    {

        z = "0";
        switch (z)
        {
            case "0":
                Debug.Log("Gracias por su Atenci�n.");
                Console.Clear();
                break;
            case "1":
                Start();
                break;
        }
    }
}